<?php

echo 'Header';


if (has_nav_menu('primary')) {
    $args = array(
        'container' => false,
        'menu_class' => 'rt-menu__main',
        'theme_location' => 'primary',
    );

    wp_nav_menu($args);
}
