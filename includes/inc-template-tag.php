<?php

function rt_get_template_part($template, $data = '')
{
    $loader = new Retheme\Template_Loader;

    $loader->set_template_data($data)
        ->get_template_part("template-parts/{$template}");

}


/*=================================================
/* MAIN TEMPLATE
/*=================================================
 * @since 1.0.0
 * @desc  this function get global template. get template part form template-parts folder
 * @param url template part $content
 */
function rt_get_template($content)
{
    get_header();

        do_action('rt_before_wrapper');

            echo '<section id="page-wrapper">';

                echo '<div id="page-container" class="page-container">';

                do_action('rt_before_content');

                if (!is_404()) {

                    if (have_posts()):

                        do_action('rt_before_loop');

                        if ($content !== 'woocommerce'): // load file if not WooCommerce page

                            while (have_posts()): the_post();

                                rt_get_template_part($content);

                            endwhile;

                        else: // load file if WooCommerce  page

                            while (woocommerce_content()): the_post();

                                woocommerce_content();

                            endwhile;

                        endif;

                        do_action('rt_after_loop');

                    else:

                        rt_get_template_part('page/content-none');

                    endif;

                } else {
                    rt_rt_get_template_part('page/content-404');
                }

                do_action('rt_after_content');

                do_action('rt_sidebar');

                echo '</div>';

            echo '</section>';

        do_action('rt_after_wrapper');

    get_footer();
}

function rt_stringfy($args){
    htmlentities(json_encode($args));
}
