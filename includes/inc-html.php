<?php

function rt_before_slider($args = array())
{
    $html = '<div class="swiper-container"><div class="swiper-wrapper">';
    echo apply_filters('rt_before_slider', $html);
}

function rt_after_slider()
{
    $html = '</div>';
    $html .= '<div class="swiper-pagination"></div>';
    $html .= '<div class="swiper-button-prev"></div>';
    $html .= '<div class="swiper-button-next"></div>';
    $html .= '<div class="swiper-scrollbar"></div>';
    $html .= '</div>';

    echo apply_filters('rt_after_slider', $html);
}

function rt_before_slider_item()
{
    $html = '<div class="swiper-slide">';
    echo apply_filters('rt_before_slider_item', $html);
}

function rt_after_slider_item()
{
    $html = '</div>';
    echo apply_filters('rt_after_slider_item', $html);
}
